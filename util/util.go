package util

import (
	c_rand "crypto/rand"
	"math/big"
)

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		result, _ := c_rand.Int(c_rand.Reader, big.NewInt(int64(len(letterRunes))))
		var _r int = int(result.Uint64())
		b[i] = letterRunes[_r]
	}
	return string(b)
}
