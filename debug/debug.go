package debug

import (
	"fmt"
	"os"
	"time"
)

func init() {

}

func Log(s string) {
	var text = fmt.Sprintf("[%s] [Log] %s \r\n", time.Now().Format("2006-01-02 15:04:05"), s)
	fmt.Printf(text)

}

func ErrorLog(s string) {
	var text = fmt.Sprintf("[%s] [ErrorLog] %s \r\n", time.Now().Format("2006-01-02 15:04:05"), s)
	fmt.Fprintf(os.Stderr, "%s", text)
}
