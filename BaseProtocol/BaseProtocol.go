package BaseProtocol

import "time"

type RetStatus struct {
	StatusCode int    `json:"StatusCode"`
	StatusMsg  string `json:"StatusMsg"`
	SystemTime int64  `json:"SystemTime"`
}

const (
	Logout                 = 9998
	UnknowError            = 9999
	Success                = 10000
	AccountExist           = 20001
	PasswordError          = 20002
	AccountNotActive       = 20003
	AccountNotExist        = 20004
	AccountorPasswordError = 20005
	CreateAccountFail      = 20006
	SendMailFail           = 20007
	InforUpdateFail        = 20008
	FileCreateFail         = 20009
	ConvertFail            = 20010
	DeductionFail          = 20011
	UpdatePointFail        = 20012
	PasswordErrorMax       = 20013
	TokenError             = 20014
	ModifyPasswordError    = 20015
	FunctionError          = 20016
	GameClose              = 20017
	GameMainten            = 20018
	PlatformNotValid       = 20019
	DomainIsZero           = 20020
	LVTooLow               = 20021
	GameURLError           = 20022
	SetTokenFail           = 20023
	CashflowFail           = 20024
	PaypalFail             = 20025
	PaypalTokenFail        = 20026
	PaypalOrderFail        = 20027
	NoPay                  = 20028
	FormatError            = 20029
	PlatformMainten        = 20030
	AmountError            = 20031
	Limitreached           = 20032
	JsonPaserError         = 30000
	DBConnectionFail       = 30001
	DBError                = 30002
	MemberCreateFail       = 30003
	RedisFail              = 30004
	NoData                 = 40001
	TooEarly               = 40002
	DataError              = 40003
	HttpError              = 40004
)

var RetStatusCode = map[int]string{
	// 20000~29999 給前端顯示用所以必須對應到多國語系
	// 30000~39999 直接顯示SERVER的問題
	// 40000~49999 開發者用
	9998:  "Logout",
	9999:  "Unknow Error",
	10000: "Success",
	20000: "Information Error",
	20001: "Account already exist",
	20002: "Account Password Error",
	20003: "Account Not Active",
	20004: "Account Not exist",
	20005: "Account or Password Error",
	20006: "Create Account Fail",
	20007: "Send Mail Fail",
	20008: "Information Update Fail",
	20009: "File Create Fail",
	20010: "Convert Fail",
	20011: "Deduction Fail",
	20012: "Update Point Fail",
	20013: "Password tried too many times",
	20014: "TokenError",
	20015: "ModifyPasswordError",
	20016: "Function Error",
	20017: "Game Close",
	20018: "Game Mainten",
	20019: "Platform Not Valid",
	20020: "Domain Is Zero",
	20021: "LV Too Low",
	20022: "Game URL Error",
	20023: "Set Token Fail",
	20024: "CashflowFail Fail",
	20025: "Paypal Fail",
	20026: "Paypal Token Fail",
	20027: "Paypal Order Fail",
	20028: "No Pay",
	20029: "Format Error",
	20030: "Platform Mainten",
	20031: "Amount Error",
	20032: "Limitreached",
	21000: "Force Redirect",
	22000: "The payment is successful",
	22001: "Insufficient account balance. Please refill it",
	30000: "Json Paser Error",
	30001: "DB Connection Fail",
	30002: "DB Error",
	30003: "Member Create Fail",
	30004: "RedisFail",
	40001: "NoData",
	40002: "Too Early",
	40003: "Data Error",
	40004: "Http Error",
}

func (retStatus *RetStatus) UpdateErrorMsg(statusCode int) {
	retStatus.StatusCode = statusCode
	retStatus.SystemTime = time.Now().Unix() * 1000
	retStatus.StatusMsg = RetStatusCode[statusCode]
}
